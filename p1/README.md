# LIS 4331 Advanced Mobile App Development

## Manolo Sanchez

### Project 1 Requirements:

*5 Parts*

1. Install pandas, pandas-datareader, and matplotlib  
2. Create a program that pulls stock price history  
3. Display the price history in code and graph form using pandas  
3. Skillset 7 Using Lists  
4. Skillset 8 Using Tuples  
5. Skillset 9 Using Sets  
6. Link to p1.ipynb file: [p1.ipynb](p1.ipynb) 

#### Assignment Screenshots:

*Screenshot of P1*:  
![p1](img/p1.png)    

*Screenshot of Jupyter*:  
![jupyter](img/p1Jupyter.png)   
   
*Screenshot of Skillset 7*:  
![ss7](img/ss7.png)  

*Screenshot of Skillset 8*:  
![ss8](img/ss8.png)  

*Screenshot of Skillset 9*:  
![ss9](img/ss9_1.png)    

*Screenshot of Skillset 9 (cont.)*:  
![ss9](img/ss9_2.png) 
