# LIS 4369 Extensible Enterprise Solutions

## Manolo Sanchez

### Project 2 Requirements:

*Three Parts*

1. Create program that conducts data analysis using R  
2. Do 20 commands using R to display characteristics about data  
3. Link to lis4369_p2_output.txt file: [lis4369_p2_output.txt](lis4369_p2_output.txt)  

#### Assignment Screenshots:

*Screenshot of RStudio*:  
![4](img/4.png)    

*Screenshot of A5 Plot 1*:  s
![plot1](img/plot1.png)   

*Screenshot of A5 Plot 2*:  
![plot2](img/plot2.png)