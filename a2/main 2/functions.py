SQ_FEET_PER_ACRE = 43560

def get_requirements():
    print("Developer: Manolo Sanchez")
    print("Miles Per Gallon")
    print("\nProgram Requirements:\n"
        + "1. Research: Convert MPG.\n"
        + "2. Must use float data type for user input and calculation.\n"
        + "3. Format and round conversion to two decimal places.\n")

    
def calculate_mpg():
    miles_driven = 0.0
    fuel_used = 0.0
    mpg = 0.0

    print("Input:")
    miles_driven = float(input("Enter miles driven: "))
    fuel_used = float(input("Enter fuel used: "))

    mpg = miles_driven / fuel_used

    print("\nOutput:")
    print("{0:,.2f} {1} {2:,.2f} {3} {4:,.2f}".format(
        miles_driven, "miles driven and", fuel_used, "gallons used = ", mpg, "mpg"))