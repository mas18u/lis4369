# LIS 4369 Extensible Enterprise Solutions

## Manolo Sanchez

### Assignment #5 Requirements:

*Four Parts*

1. Create program that conducts data analysis using R  
2. Skillset 13 Sphere volume calculator  
3. Skillset 14 Calculator with error handling  
4. Skillset 15 File write/read  
6. Link to lis4369_a5.R file: [lis4369_a5.R](lis4369_a5.R)  

#### Assignment Screenshots:

*Screenshot of A5 1*:  
![a5_1](img/A5_1.png)    

*Screenshot of A5 2*:  
![a5_2](img/A5_2.png)   

*Screenshot of Skillset 13  
![ss13](img/13.png)

*Screenshot of Skillset 14  
![ss14](img/14.png)

*Screenshot of Skillset 15  
![ss15](img/15.png)
