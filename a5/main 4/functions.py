import os

lincoln_address = ""

def get_requirements():
    print("File write read\n")
    print("\nProgram Requirements:\n"
                + "1. Create write_read_flie subdirectory with twi files: main.py and fuctions.py.\n"
                + "2. Use President Abraham Lincoln's Gettysburg Address: Full Text.\n"
                + "3. Read address from same file.\n"
                + "4. Create Python docstrings for each function in functions.py file.\n"
                + "5. Display Python Docstrings.\n"
                + "6. Display full file path.\n"
                + "7. Replicate display below.\n\n"
                + "Help on function write_read_file in module functions:\n\n"
                + "write_read_file()\n Usage: calls two functions:\n1.file_write() #writes to file.\n2.file_read() # reads from file.\nParameters: none\nReturns: none\n\n"
                + "Help on function file_write in module functions:\n\n"
                + "file_write()\nUsage: creates file, and writes contents of global variable to file.\n"
                + "Parameters: none\nReturns: none\n\n"
                +"Help on function file_read in module functions:\n\n"
                + "file_read()\nUsage: reads contents of written file\nParameters: none\nReturns: none\n\n"
                + "President Abraham Lincoln's Gettysburg Address:\n")

def file_write():
    
    f = open("tests.txt", "w")
    f.write(" Four score and seven years ago our fathers brought forth on this continent, a new nation, conceived in Liberty, and dedicated to the proposition that all men are created equal." +
                "Now we are engaged in a great civil war, testing whether that nation, or any nation so conceived and so dedicated, can long endure. We are met on a great battle-field of that war. We have come to dedicate a portion of that field, as a final resting place for those who here gave their lives that that nation might live. It is altogether fitting and proper that we should do this." +
                "But, in a larger sense, we can not dedicate -- we can not consecrate -- we can not hallow -- this ground. The brave men, living and dead, who struggled here, have consecrated it, far above our poor power to add or detract. The world will little note, nor long remember what we say here, but it can never forget what they did here. It is for us the living, rather, to be dedicated here to the unfinished work which they who fought here have thus far so nobly advanced. It is rather for us to be here dedicated to the great task remaining before us -- that from these honored dead we take increased devotion to that cause for which they gave the last full measure of devotion -- that we here highly resolve that these dead shall not have died in vain -- that this nation, under God, shall have a new birth of freedom -- and that government of the people, by the people, for the people, shall not perish from the earth. ")
        
    f.write("\n\nFull File Path: ")
    f.write(os.path.realpath(f.name))
    f.close()

def file_read():
    f = open("tests.txt", "r")
    print(f.read())

def write_read_file():

    file_write()
    file_read()
