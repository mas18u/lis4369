# LIS 4369 Extensible Enterprise Solutions

## Manolo Sanchez

### LIS 4369 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")  
		- Install Python  
		- Install R  
		- Install R Studio  
		- Install Visual Studio Code  
		- Create a1_tip_calculator application  
		- Create a1 tip calculator Jupyter Notebook  
		- Provide screenshots of installations  
		- Create Bitbucket repo  
		- Complete Bitbucket tutorial (bitbucketstationlocations)  
		- Provide git command descriptions  

2. [A2 README.md](a2/README.md "My A2 README.md file")  
		- Python program to calculate paycheck amount  
		- Skillset 1 Sq Feet To Acres  
		- Skillset 2 Miles Per Gallon  
		- Skillset 3 IT/ICT Student Percentage  

3. [A3 README.md](a3/README.md "My A3 README.md file")  
		- Python program to calculate interior home paint cost  
		- Uses loop structure  
		- Skillset 4 Calorie Percentage  
		- Skillset 5 Python Selection Structures  
		- Skillset 6 Pyhton Loops  
		
4. [P1 README.md](p1/README.md "My P! README.md file")  
		- Install pandas, pandas-datareader, and matplotlib  
		- Create a program that pulls stock price history  
		- Display the price history in code and graph form using pandas    
		- Skillset 7 Using Lists  
		- Skillset 8 Using Tuples  
		- Skillset 9 Using Sets  

5. [A4 README.md](a4/README.md "My A4 README.md file")  
		- Create program that conducts data analysis from data taken from an online source.  
		- Skillset 10 dictionaries  
		- Skillset 11 random number generator  
		- Skillset 12 temperature conversion program  

6. [A5 README.md](a5/README.md "My A5 README.md file")  
		- Create program that conducts data analysis using R  
		- Skillset 13 Sphere volume calculator  
		- Skillset 14 Calculator with error handling  
		- Skillset 15 File write/read  
7. [A2 README.md](p2/README.md "My A2 README.md file")  
		- Create program that conducts data analysis using R  
		- Do 20 commands using R to display characteristics about data  

