import functions as f

def main():

        f.get_requirements()
        f.calculate_sqft_to_acres()

if __name__ == "__main__":
        main()