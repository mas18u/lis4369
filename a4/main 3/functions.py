import random

def get_requirements():
    print("\nProgram Requirements:\n"
                + "1. Dictionaries (Python data structure): unordered key:value pairs.\n"
                + "2. Dictionary: an associative array (also known as hashes).\n"
                + "3. Any key in dictionary is associated (or mapped) to a value (i.e, any Python data type).\n"
                + "4. Keys: must be of immutable type (string, number or tuple with immutable elements) and must be unique.\n"
                + "5. Values: can be any data type and can repeat.\n"
                + "6. Create a program that mirrors the following IPO (input/process/output) format.\n"
                + "\tCreate empty dictionary, using curly braces {}: my_dictionary = []"
                + "\tUse the following keys: fname, lname, degree, major, gpa\n"
                + "Note: Dictionaries have key-value pairs instead of single values; this differentiates a dictionary from a set\n"
                )

def random_function():

    start = 0
    end = 0

    print("Input: ")
    start = int(input("Enter beginning value: "))
    end = int(input("Enter ending value: "))

    print("\nOutput:")
    print("Example 1: Using range() and randint() functions:")
    # Here, range just used to print 10 numbers
    # However, randint(start, end) used to print range of integers (inclusive)
    for item in range(10):
        print(random.randint(start, end), sep=", ", end=" ")

    print()

    print("Example 2: Using a list, with range() and shuffle() functions:")
    # shuffle() reorganizes order of items in a sequence (e.g., list)
    # range generates range of integers, "end" is noninclusive, must add 1
    # Can't use range() by itself, must convert to list
    my_list = list(range(start, end + 1))
    random.shuffle(my_list) # reorganize list item
    for item in my_list:
        print(item, sep=", ", end=" ")
    print()