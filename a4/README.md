# LIS 4369 Extensible Enterprise Solutions

## Manolo Sanchez

### Assignment #4 Requirements:

*Four Parts*

1. Create program that conducts data analysis from data taken from an online source.  
2. Skillset 10 dictionaries  
3. Skillset 11 random number generator  
4. Skillset 12 temperature conversion program  
6. Link to a4.ipynb file: [a4.ipynb](a4.ipynb)  

#### Assignment Screenshots:

*Screenshot of A4*:  
![a4](img/a4.png)    

*Screenshot of Jupyter*:  
![jupyter](img/jupyter.png)   

*Screenshot of Skillset 10  
![ss10](img/10.png)

*Screenshot of Skillset 11  
![ss11](img/11.png)

*Screenshot of Skillset 12  
![ss12](img/12.png)
