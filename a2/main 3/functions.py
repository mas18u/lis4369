SQ_FEET_PER_ACRE = 43560

def get_requirements():
    print("Developer: Manolo Sanchez")
    print("IT/ICT Student Percentage")
    print("\nProgram Requirements:\n"
        + "1. Find number of IT/ICT students in class.\n"
        + "2. Calculate IT/ICT student percentage.\n"
        + "3. Must use float data type (to facilitate right-alignment)\n"
          "4. Format, right-allign numbers, and round to two decimal places.\n")

    
def calculate_percentage():
    IT_students = 0.00
    ICT_students = 0.00

    print("Input:")
    IT_students = float(input("Enter number of IT students: "))
    ICT_students = float(input("Enter number of ICT students: "))

    print("\nOutput:")
    print(f"Total Students: {(IT_students + ICT_students):>8.2f}")
    print(f"IT Students: {(IT_students/(IT_students+ICT_students) * 100):>8.2f}%")
    print(f"ICT Students: {(ICT_students/(IT_students+ICT_students) * 100):>8.2f}%")

     

    
