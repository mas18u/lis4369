# LIS 4369 Extensible Enterprise Solutions

## Manolo Sanchez

### Assignment #3 Requirements:

*Four Parts*

1. Python program to calculate interior home paint cost
2. Uses loop structure
3. Skillset 4 Calorie Percentage  
4. Skillset 5 Python Selection Structures
5. Skillset 6 Python Loops
6. Link to a3.ipynb file: [a3.ipynb](a3.ipynb)

#### Assignment Screenshots:

*Screenshot of A3*:  
![a3](img/a3.png)    

*Screenshot of Jupyter*:  
![jupyter](img/jupyter.png)   

*Screenshot of Jupyter 2*:  
![jupyter2](img/jupyter2.png)   
 s
*Screenshot of Skillset 4*:  
![ss1](img/ss4.png)  

*Screenshot of Skillset 5*:  
![ss2](img/ss5.png)  

*Screenshot of Skillset 6*:  
![ss3](img/ss6.png)    
