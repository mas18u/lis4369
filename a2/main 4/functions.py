
def get_requirements():
    print("Developer: Manolo Sanchez")
    print("Payroll Calculator")
    print("\nProgram Requirements:\n"
        + "1. Must use float data type for user input.\n"
        + "2. Overtime rate: 1.5 time hourly rate (hours over 40).\n"
        + "3. Holiday rate 2.0 times hourly rate (all holiday hours)\n"
          "4. Must format currency with dollar sign, and round to two decimal places.\n"
          "5. Create at least three funcitons that are called by the program.\n"
          "\ta. main(): calls at least two other functions.\n"
          "\tb. get_requirements(): displays the program requirements.\n"
          "\tc. calculate_payroll(): calculates an individual one-week paycheck.\n")

    
def calculate_payroll():
    hours_worked = 0.00
    holiday_hours = 0.00
    hourly_pay_rate = 0.00

    print("Input:")
    hours_worked = float(input("Enter hours worked: "))
    holiday_hours = float(input("Enter holiday hours: "))
    hourly_pay_rate = float(input("Enter hourly pay rate: "))

    print("\nOutput:")
    
    if hours_worked > 40: 
        print(f"Base: ${(40 * hourly_pay_rate):,.2f}")
        print(f"Overtime: ${((hours_worked - 40)*(hourly_pay_rate * 1.5)):,.2f}")
    else:
        print(f"Base: ${(hours_worked * hourly_pay_rate):,.2f}")
        print("Overtime: $0.00")

    print(f"Holiday: ${((holiday_hours * (hourly_pay_rate * 2))):,.2f}")
    if hours_worked > 40:
        print(f"Gross: ${((40 * hourly_pay_rate) + ((hours_worked - 40)*(hourly_pay_rate * 1.5)) + ((holiday_hours * (hourly_pay_rate * 2)))):,.2f}")
    else:
        print(f"Gross: ${((hours_worked * hourly_pay_rate) + ((holiday_hours * (hourly_pay_rate * 2)))):,.2f}")


     
    
    
