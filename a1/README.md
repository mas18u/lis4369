# LIS 4369 Extensible Enterprise Solutions

## Manolo Sanchez

### Assignment #1 Requirements:

*Four Parts*

1. Distributed version control with git and bitbucket
2. development installations
3. Chapter questions
4. Bitbucket repo links a) This assignment b) the completed tutorials above (bitbucket station locations and myteamquotes)

#### README.md file should include the following items:

* screenshot of a1_tip_calculator app running
* Link to A1.ipynb file: [tip_calculator.ipynb](a1_tip_calculator/a1_tip_calculator.ipynb)
* git commands w/ short descriptions

> #### Git commands w/short descriptions:

1. git init - creates an empty git repository
2. git status - shows the working tree status
3. git add - adds file contents to the index
4. git commit - record changes to the repository
5. git push - update remote refs along with associated objects
6. git pull - fetch from and integrate with another repository or a local branch
7. git clone - downloads existing source code from a remote repository

#### Assignment Screenshots:

*Screenshot of a1_tip_calculator app running (Visual studio code)*:  
![img](img/running.png)  

*A1 Jupyter Notebook:*:  
![img](img/jupyter.png)  
#### Tutorial Links:  
*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/mas18u/bitbucketstationlocations/src/master/ "Bitbucket Station Locations")
