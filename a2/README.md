# LIS 4369 Extensible Enterprise Solutions

## Manolo Sanchez

### Assignment #2 Requirements:

*Four Parts*

1. Python program to calculate paycheck amount  
2. Skillset 2 Miles Per Gallon  
3. Skillset 3 IT/ICT Student Percentage  
4. Skillset 1 Sq Feet To Acres  
5. Link to A2.ipynb file: [A2.ipynb](A2.ipynb)

#### Assignment Screenshots:



*Screenshot of A2 No Overtime*:  
![no](img/noOvertime.png)   

*Screenshot of A2 Overtime*:  
![overtime](img/overtime.png)   

*Screenshot of Jupyter 1*:  
![jupyter1](img/jupyter1.png)   

*Screenshot of Jupyter 2*:  
![jupyter1](img/jupyter2.png)   

*Screenshot of Jupyter 3*:  
![jupyter1](img/jupyter3.png)   

*Screenshot of Skillset 1*:  
![ss1](img/ss1.png)  

*Screenshot of Skillset 2*:  
![ss2](img/ss2.png)  

*Screenshot of Skillset 3*:  
![ss3](img/ss3.png)    
